#include "vectoreuc.h"
#include <vector>
using namespace std;

//default constructor
// crds is a vector of doubles, probably size of dimension
vectoreuc::vectoreuc() {}

//constructor with n elements
vectoreuc::vectoreuc(int n) {
  crds.resize(n);
}

//returns vector number of vector elements
int vectoreuc::size() {
  return crds.size();
}

//returns nth elements of vector inside, overloaded
double& vectoreuc::operator[](int idx) {
  return crds[idx];
}

//element by element vector difference overloaded on -
vectoreuc vectoreuc::operator-(const vectoreuc& rhs) {
  vectoreuc res(crds.size());
  for(int i=0; i<crds.size(); i++) {
    res[i] = crds[i] - rhs.crds[i];
  }
  return res;
}
// element by element vector addition overloaded on +
vectoreuc vectoreuc::operator+(const vectoreuc& rhs) {
  vectoreuc res(crds.size());
  for(int i=0; i<crds.size(); i++) {
    res[i] = crds[i] + rhs.crds[i];
  }
  return res;
}

// divide all elements by s overloaded on /
vectoreuc vectoreuc::operator/(const double& s) {
  vectoreuc res(crds.size());
  for(int i=0; i<crds.size(); i++) {
    res[i] = crds[i] / s;
  }
  return res;
}

//take sqrt of l2 norm of vector, divide all elements to it
vectoreuc vectoreuc::normalized() {
  vectoreuc res(crds.size());
  double length = 0;
  for(int i=0; i<crds.size(); i++) {
    length += crds[i] * crds[i];
  }
  length = sqrt(length);
  for(int i=0; i<crds.size(); i++) {
    res[i] = crds[i] / length;
  }
  return res;
}

// take the dot of two vectors
double vectoreuc::dot(vectoreuc& rhs) const {
  double res = 0;
  for(int i=0; i<crds.size(); i++) {
    res += crds[i] * rhs[i];
  }
  return res;
}

// overload << to print all values of vector
ostream& operator<<(ostream& out, const vectoreuc& vec) {
  out << "(";
  for(int i=0; i<vec.crds.size()-1; i++) {
    out << vec.crds[i] << ",";
  }
  out << vec.crds[vec.crds.size()-1] <<  ")";
  return out;
}

// return l2 norm of vector
double vectoreuc::L2norm() {
  double length = 0;
  for(int i=0; i<crds.size(); i++) {
    length += crds[i] * crds[i];
  }
  return sqrt(length);
}
//fills the vector with zeros
void vectoreuc::zero() {
  for(int i=0; i<crds.size(); i++)
    crds[i] = 0;
}

//returns a new vector scaled by rhs
vectoreuc vectoreuc::operator*(double rhs) {
  vectoreuc res(crds.size());
  for(int i=0; i<res.size(); i++) {
    res[i] = rhs * crds[i];
  }
  return res;
}
