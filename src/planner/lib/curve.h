#ifndef PATHREPLAN_CURVE_H
#define PATHREPLAN_CURVE_H
#include <vector>
#include "vectoreuc.h"
using namespace std;
class curve {
  /*
    bezier defined between t [0,1]
    use duration to scale
  */
  public:
    /* curve is in this dimension */
    int dimension;
    /* control points of the curve, each have size dimension */
    vector<vectoreuc> cpts;
    /* duration of the curve */
    double duration;
    //init curve with duration and dimension
    curve(double dur, int dim);
    // add control point to curve
    void add_cpt(vectoreuc& cpt);
    // number of ctrol points
    int size();
    // return n th control point
    vectoreuc& operator[](int idx);
    // set duration of curve
    void set_duration(double dur);
    // if t is out of duration, return end ctrl point of the curve
    // else eval curve at time t
    vectoreuc eval(double t);
    // return nth derivative of curve at time t 
    vectoreuc neval(double t, int n);
    // calculates n choose k
    static int comb(int n, int i);
    // integrate curve using gradient, prolly not used
    double integrate(double from, double to, vector<double>& grad);
    // returns elementwise difference of control points of two curves
    curve& operator-=(const curve& rhs);
    // prints control points of the curve
    void print();
};

#endif
