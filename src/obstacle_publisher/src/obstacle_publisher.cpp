#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <obstacle_publisher/ObstacleArrayStamped.h>
#include "obstacle_finder.h"



int main(int argc, char **argv)
{
  ros::init(argc, argv, "obstacle_publisher");
  ros::NodeHandle nodehandle("~");
  //std::string map_topic;
  //n.getParam("map_topic", map_topic);
  ObstacleFinder obsfinder("map");
  ros::Rate r(1);
  ros::spin();
  return 0;
}