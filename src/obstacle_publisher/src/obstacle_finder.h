#ifndef OBSTACLE_FINDER_H
#define OBSTACLE_FINDER_H
#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <vector>
#include <tuple>
#include <obstacle.h>
#include <string>


class ObstacleFinder {
  public:
    std::string map_topic;
    ros::Publisher m_pubObstacle;
    ros::Subscriber m_subMap;
    ros::Publisher m_pubPoseArray;
    std::vector<obstacle2D> obstacles_result;
    int obs_size;  
    ros::NodeHandle nh;
    ros::NodeHandle th;  
    std::vector<std::tuple<double,double>> raw_obstacles;
    ObstacleFinder (const std::string& map_topic);
    ObstacleFinder (const std::string& map_topic,ros::NodeHandle nho);

  private:  
    void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);

};

#endif
