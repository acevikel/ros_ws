#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <obstacle_publisher/ObstacleArrayStamped.h>
#include "obstacle_finder.h"
#include <vectoreuc.h>
#include <obstacle.h>
#include <vector>


ObstacleFinder::ObstacleFinder (const std::string& map_topic)
    : map_topic(map_topic)
{
ros::NodeHandle th;
m_pubObstacle = th.advertise<obstacle_publisher::ObstacleArrayStamped>("obstacles", 1,true);
m_subMap= th.subscribe(map_topic, 1, &ObstacleFinder::mapCallback, this);
m_pubPoseArray = th.advertise<geometry_msgs::PoseArray>("pose_current", 1);
}

ObstacleFinder::ObstacleFinder (const std::string& map_topic,ros::NodeHandle nho)
    : map_topic(map_topic) , th(nho)
{
m_pubObstacle = th.advertise<obstacle_publisher::ObstacleArrayStamped>("obstacles", 1,true);
m_subMap= th.subscribe(map_topic, 1, &ObstacleFinder::mapCallback, this);
m_pubPoseArray = th.advertise<geometry_msgs::PoseArray>("pose_current", 1);
}


void ObstacleFinder::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg) {
    raw_obstacles.clear();
   // std::cout << "RUNNNNEEEDDD CB" << std::endl;
    geometry_msgs::Point p1,p2,p3,p4;
    geometry_msgs::Pose testP;
    geometry_msgs::PoseArray testPA;
    obstacle_publisher::ObstacleArrayStamped obstacleArray;
    vectoreuc pt1(2);
    vectoreuc pt2(2);
    vectoreuc pt3(2);
    vectoreuc pt4(2);
    vector<obstacle2D> obstacle_vector_temp;
    //obstacle2D o;
    int num_obs =0;
    int width,height,current_data;
    double res,curx,cury,orig_x,orig_y;
    orig_x = msg->info.origin.position.x;
    orig_y = msg->info.origin.position.y;
    res = msg->info.resolution;
    width = msg->info.width;
    height = msg->info.height;
    for (int i=0;i<height;i++) {
        for (int j=0;j<width;j++) {
        current_data = msg->data[i*width+j];
        if (current_data==100){
          obstacle2D o;
            num_obs++;
            obstacle_publisher::Obstacle obstacle;
            curx = orig_x+res*j;
            cury = orig_y+res*i;
            p1.x = curx;
            p1.y = cury;
            pt1[0]=p1.x;
            pt1[1]=p1.y;
            raw_obstacles.push_back(make_tuple(p1.x,p1.y));
            o.add_pt(pt1);
            p2.x = curx;
            p2.y = cury+res;
            pt2[0]=p2.x;
            pt2[1]=p2.y;
            raw_obstacles.push_back(make_tuple(p2.x,p2.y));
            o.add_pt(pt2);
            p3.x = curx+res;
            p3.y = cury+res;
            pt3[0]=p3.x;
            pt3[1]=p3.y;
            raw_obstacles.push_back(make_tuple(p3.x,p3.y));
            o.add_pt(pt3);
            p4.x = curx+res;
            p4.y = cury;
            pt4[0]=p4.x;
            pt4[1]=p4.y;
            raw_obstacles.push_back(make_tuple(p4.x,p4.y));
            o.add_pt(pt4);
            o.convex_hull();
            o.ch_planes(0);
            obstacle_vector_temp.push_back(o);
            obstacle.points.push_back(p1);
            obstacle.points.push_back(p2);
            obstacle.points.push_back(p3);
            obstacle.points.push_back(p4);
            obstacleArray.obstacles.push_back(obstacle);

            testP.position.x = curx;
            testP.position.y = cury;
            testPA.poses.push_back(testP);
        }

        }
    }
    obstacles_result=obstacle_vector_temp;
    obs_size = num_obs;
    obstacleArray.header.frame_id = "odom";
    obstacleArray.header.stamp = ros::Time::now();
    m_pubObstacle.publish(obstacleArray);

    testPA.header.frame_id = "odom";
    testPA.header.stamp = ros::Time::now();
    m_pubPoseArray.publish(testPA);
}
