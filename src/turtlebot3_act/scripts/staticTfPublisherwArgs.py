#!/usr/bin/env python
import rospy
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped
import sys

rname = str(sys.argv[1])
rospy.init_node("rname_static_tf")
tm = TFMessage()
tfm= TransformStamped()
tfm.header.frame_id = "world"
tfm.child_frame_id = rname+"/odom"
tfm.transform.rotation.w = 1.0
tf_pub = rospy.Publisher("tf",TFMessage,queue_size=1)
r=rospy.Rate(50)

while not rospy.is_shutdown():
    tfm.header.stamp = rospy.Time.now()
    tm.transforms = [tfm]
    tf_pub.publish(tm)
    r.sleep()
