python $HOME/ros_ws/src/turtlebot3_act/scripts/stop_tb3.py
rosservice call /gazebo/set_model_state '{model_state: { model_name: turtlebot3_burger, pose: { position: { x: 0.0, y: 0.0 ,z: 0.0 }, orientation: {x: 0, y: 0, z: 0, w: 0 } }, twist: { linear: {x: 0.0 , y: 0 ,z: 0 } , angular: { x: 0.0 , y: 0 , z: 0.0 } } , reference_frame: world } }'
rosservice call /octomap_server/reset
