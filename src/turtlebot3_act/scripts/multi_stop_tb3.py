#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from copy import deepcopy



def odom_sub_1(data):
    global old_x_1,old_y_1,init_1,cont_1
    if init_1:
        old_x_1 = data.pose.pose.position.x
        old_y_1 = data.pose.pose.position.x
        init_1 = False
        return
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y
    if abs(old_x_1-x)>0.0001 and abs(old_y_1-y)>0.0001:
        cmd_pub_1.publish(Twist())
    else:
        cont_1 = False
    old_x_1 = x
    old_y_1 = y 

def odom_sub_2(data):
    global old_x_2,old_y_2,init_2,cont_2
    if init_2:
        old_x_2 = data.pose.pose.position.x
        old_y_2 = data.pose.pose.position.x
        init_2 = False
        return
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y
    if abs(old_x_2-x)>0.0001 and abs(old_y_2-y)>0.0001:
        cmd_pub_2.publish(Twist())
    else:
        cont_2 = False
    old_x_2 = x
    old_y_2 = y

init_1 = init_2 = True
cont_1 = cont_2 = True
rospy.init_node('stoptb3sim')
rospy.Subscriber('tb3_one/odom',Odometry,callback=odom_sub_1)
rospy.Subscriber('tb3_two/odom',Odometry,callback=odom_sub_2)
cmd_pub_1 = rospy.Publisher('tb3_one/cmd_vel',Twist,queue_size=1)
cmd_pub_2 = rospy.Publisher('tb3_two/cmd_vel',Twist,queue_size=1)
r = rospy.Rate(10)
while not rospy.is_shutdown():
    if (not cont_1) and (not cont_2): break
    r.sleep()
