python $HOME/ros_ws/src/turtlebot3_act/scripts/multi_stop_tb3.py
rosservice call /gazebo/set_model_state '{model_state: { model_name: tb3_one, pose: { position: { x: -2.0, y: 0.0 ,z: 0.0 }, orientation: {x: 0, y: 0, z: 0.7412965, w: 0.6711777 } }, twist: { linear: {x: 0.0 , y: 0 ,z: 0 } , angular: { x: 0.0 , y: 0 , z: 0.0 } } , reference_frame: world } }'
rosservice call /gazebo/set_model_state '{model_state: { model_name: tb3_two, pose: { position: { x: 2.0, y: 0.0 ,z: 0.0 }, orientation: {x: 0.0, y: 0.0, z: -0.7, w: 0.7 } }, twist: { linear: {x: 0.0 , y: 0 ,z: 0 } , angular: { x: 0.0 , y: 0 , z: 0.0 } } , reference_frame: world } }' 
rosservice call /tb3_one_octomap_server/reset
rosservice call /tb3_two_octomap_server/reset
