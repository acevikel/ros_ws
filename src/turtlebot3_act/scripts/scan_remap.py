#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from copy import deepcopy



def scan_sub_1(data):
  data.range_max=32
  data.ranges = [x if x!=0 else 30.0 for x in data.ranges] 
  scan_pub_1.publish(data)


def scan_sub_2(data):
  data.range_max=32
  data.ranges = [x if x!=0 else 30.0 for x in data.ranges] 
  scan_pub_2.publish(data)

rospy.init_node('laserremapper')
rospy.Subscriber('tb3_one/scan',LaserScan,callback=scan_sub_1)
rospy.Subscriber('tb3_two/scan',LaserScan,callback=scan_sub_2)
scan_pub_1 = rospy.Publisher('tb3_one/scan_remapped',LaserScan,queue_size=20)
scan_pub_2 = rospy.Publisher('tb3_two/scan_remapped',LaserScan,queue_size=20)
rospy.spin()



