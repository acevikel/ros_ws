#!/usr/bin/env python
import rospy
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped


def tf_cb(data):
	for cur_tf in data.transforms:
		cur_tf.child_frame_id = cur_tf.child_frame_id+"/base_footprint"
	tf_pub.publish(data)

rospy.init_node("tf_remapper")
rospy.Subscriber("tf_vrpn",TFMessage,tf_cb)
tf_pub = rospy.Publisher("tf",TFMessage,queue_size=1)
rospy.spin()

