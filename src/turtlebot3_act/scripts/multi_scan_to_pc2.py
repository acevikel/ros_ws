#!/usr/bin/env python
import sensor_msgs.point_cloud2 as pc2
import rospy
from sensor_msgs.msg import PointCloud2, LaserScan
import laser_geometry.laser_geometry as lg
import math

rospy.init_node("laserscan_to_pointcloud")

lp = lg.LaserProjection()

pc_pub_1 = rospy.Publisher("tb3_one/converted_pc", PointCloud2, queue_size=1)
pc_pub_2 = rospy.Publisher("tb3_two/converted_pc", PointCloud2, queue_size=1)

def scan_cb_1(msg):
    # convert the message of type LaserScan to a PointCloud2
    msg.range_max=32 #remap
    msg.ranges = [x if x!=0 else 10.0 for x in msg.ranges] #remap 
    pc2_msg = lp.projectLaser(msg)
    pc2_msg.header.frame_id = "tb3_one/base_scan"
    pc_pub_1.publish(pc2_msg)

def scan_cb_2(msg):
    # convert the message of type LaserScan to a PointCloud2
    msg.range_max=32 #remap
    msg.ranges = [x if x!=0 else 10.0 for x in msg.ranges] #remap 
    pc2_msg = lp.projectLaser(msg)
    pc2_msg.header. frame_id = "tb3_two/base_scan"
    pc_pub_2.publish(pc2_msg)

rospy.Subscriber("tb3_one/scan", LaserScan, scan_cb_1, queue_size=1)
rospy.Subscriber("tb3_two/scan", LaserScan, scan_cb_2, queue_size=1)

rospy.spin()
