#!/usr/bin/env python
# license removed for brevity
import rospy
import time
import signal
import sys
from geometry_msgs.msg import PolygonStamped
from obstacle_publisher.msg import ObstacleArrayStamped

def generate_unit_rectangle_poly(obstacle,color="g",id=0):
        poly = PolygonStamped()
        poly.header.stamp = rospy.Time.now()
        poly.header.frame_id = "odom"
        poly.polygon.points = obstacle.points
        return poly

def signal_handler(signal, frame):
    global cont
    print "Sig handler invoked"
    rospy.signal_shutdown("Done")
    cont = False


def obstacle_cb(obsArray):
    for obstacle in obsArray.obstacles:
        poly = generate_unit_rectangle_poly(obstacle)
        time.sleep(0.3)
        pub.publish(poly)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGHUP, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)



rospy.init_node('obstacleVisualizer')
pub = rospy.Publisher('/polygons', PolygonStamped, queue_size=5)
rospy.Subscriber("obstacles",ObstacleArrayStamped,callback=obstacle_cb)
rospy.spin()