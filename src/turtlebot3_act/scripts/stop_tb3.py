#!/usr/bin/env python
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from copy import deepcopy



def odom_sub(data):
    global old_x,old_y,init,cont
    if init:
        old_x = data.pose.pose.position.x
        old_y = data.pose.pose.position.x
        init = False
        return
    x = data.pose.pose.position.x
    y = data.pose.pose.position.y
    if abs(old_x-x)>0.000001 and abs(old_y-y)>0.000001:
        cmd_pub.publish(Twist())
    else:
        cont = False
    old_x = x
    old_y = y 

init = True
cont = True
rospy.init_node('stoptb3sim')
rospy.Subscriber('/odom',Odometry,callback=odom_sub)
cmd_pub = rospy.Publisher('cmd_vel',Twist,queue_size=1)
r = rospy.Rate(10)
while not rospy.is_shutdown():
    if not cont: break
    r.sleep()
