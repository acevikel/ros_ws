#!/usr/bin/env python
import rospy
from tf2_msgs.msg import TFMessage
from geometry_msgs.msg import TransformStamped
from nav_msgs.msg import Odometry

def odomcb(data):
	tm = TFMessage()
	tfm= TransformStamped()
	tfm.header.frame_id = "odom"
	tfm.child_frame_id = "base_footprint"
	tfm.transform.rotation.x = data.pose.pose.orientation.x
	tfm.transform.rotation.y = data.pose.pose.orientation.y    
	tfm.transform.rotation.z = data.pose.pose.orientation.z    
	tfm.transform.rotation.w = data.pose.pose.orientation.w
	tfm.transform.translation.x = data.pose.pose.position.x        
	tfm.transform.translation.y = data.pose.pose.position.y
	tfm.transform.translation.z = data.pose.pose.position.z
	tfm.header.stamp = rospy.Time.now()
	tm.transforms = [tfm]
	tf_pub.publish(tm)


       
rospy.init_node("static_tf")
tm = TFMessage()
tfm= TransformStamped()
tfm.header.frame_id = "world"
tfm.child_frame_id = "odom"
tfm.transform.rotation.w = 1.0
tf_pub = rospy.Publisher("tf",TFMessage,queue_size=1)
rospy.Subscriber("/odom",Odometry,odomcb)

r=rospy.Rate(50)

while not rospy.is_shutdown():
    tfm.header.stamp = rospy.Time.now()
    tm.transforms = [tfm]
    tf_pub.publish(tm)
    r.sleep()

