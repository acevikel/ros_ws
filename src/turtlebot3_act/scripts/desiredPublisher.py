import rospy
from nav_msgs.msg import Odometry
from create2_controller.msg import TrajectoryState2D
from tf.transformations import euler_from_quaternion,quaternion_from_euler
from math import atan,pi

def normalize_ang(theta):
    if theta>pi:
        theta=-1*(2*pi-theta)
    elif theta<-pi:
        theta+=2*pi
    return theta

def odom_cb(data):
    odom_msg = Odometry()
    qx=data.pose.pose.orientation.x
    qy=data.pose.pose.orientation.y
    qz=data.pose.pose.orientation.z
    qw=data.pose.pose.orientation.w
    yaw=euler_from_quaternion([qx,qy,qz,qw])[2]
    #quat = quaternion_from_euler(0,0,theta)
    yaw = normalize_ang(yaw)
    odom_msg.pose.pose.position.x = data.pose.pose.position.x
    odom_msg.pose.pose.position.y = data.pose.pose.position.y
    odom_msg.pose.pose.position.z = yaw
    odom_msg.header.stamp = rospy.Time.now()
    actual_odom_pub.publish(odom_msg)

def get_ang_from_v(vx,vy):
    if vx!=0.0:
        return atan(vy/vx)
    else:
        return 0.0    
def traj_cb(data):
    global init,old_theta,theta
    px = data.position.x
    py = data.position.y
    vx = data.velocity.x
    vy = data.velocity.y
    if init:
        old_theta = get_ang_from_v(vx,vy)
        init = False
        return 
    d_theta = get_ang_from_v(vx,vy)-old_theta
    if d_theta>pi/2.0:
        theta-=pi
    elif d_theta<-pi/2:
        theta+=pi
    theta+=d_theta
    if theta>pi:
        theta-=2*pi
    elif theta<-pi:
        theta+=2*pi
    odom_msg = Odometry()
    odom_msg.pose.pose.position.x = px
    odom_msg.pose.pose.position.y = py
    odom_msg.pose.pose.position.z = theta
    #odom_msg.pose.pose.orientation.x = quat[0]
    #odom_msg.pose.pose.orientation.y = quat[1]
    #odom_msg.pose.pose.orientation.z = quat[2]
    #odom_msg.pose.pose.orientation.w = quat[3]
    odom_msg.header.stamp = rospy.Time.now()
    odom_pub.publish(odom_msg)
    old_theta = theta
    
init = True
theta = 0.0    
rospy.init_node('desired_publisher')
rospy.Subscriber('desired_state',TrajectoryState2D,traj_cb)
rospy.Subscriber('odom',Odometry,odom_cb)
odom_pub=rospy.Publisher('desired_odom',Odometry,queue_size=1)
actual_odom_pub=rospy.Publisher('actual_odom',Odometry,queue_size=1)

rospy.spin()