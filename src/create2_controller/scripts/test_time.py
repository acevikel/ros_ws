import rospy
from create2_controller.msg import TrajectoryState2D
import ugv_trajectory
import time

rospy.init_node('some_tests')
r = rospy.Rate(10) # hz
start = rospy.get_time()
while start == 0.0:
    time.sleep(1)
    start = rospy.get_time()
while not rospy.is_shutdown():
    now = rospy.get_time()
    t = (now - start)#.to_sec()#/40.0
    print t
    r.sleep()