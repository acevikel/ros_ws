#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import argparse

import ugv_trajectory

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("trajectory", type=str, help="CSV file containing trajectory")
  parser.add_argument("--stretchtime", type=float, help="stretch time factor (smaller means faster)")
  args = parser.parse_args()

  traj = ugv_trajectory.Trajectory()
  traj.loadcsv(args.trajectory)

  if args.stretchtime:
    traj.stretchtime(args.stretchtime)

  ts = np.arange(0, traj.duration, 0.01)
  evals = np.empty((len(ts), 8))
  for t, i in zip(ts, range(0, len(ts))):
    e = traj.eval(t)
    evals[i, 0:2]  = e.pos
    evals[i, 2:3]  = e.theta
    evals[i, 3:5]  = e.vel
    evals[i, 5:6]  = e.omega
    evals[i, 6:8]  = e.acc

  velocity = np.linalg.norm(evals[:,3:6], axis=1)
  acceleration = np.linalg.norm(evals[:,6:8], axis=1)

  # print stats
  print("max speed (m/s): ", np.max(velocity))
  print("max omega (rad/s): ", np.max(evals[:,5]))
  print("max acceleration (m/s^2): ", np.max(acceleration))

  # Create 3x1 sub plots
  gs = gridspec.GridSpec(6, 1)
  fig = plt.figure()

  ax = plt.subplot(gs[0:2, 0]) # row 0
  ax.plot(evals[:,0], evals[:,1])

  ax = plt.subplot(gs[2, 0]) # row 3
  ax.plot(ts, evals[:,2])
  ax.set_ylabel("theta [rad]")

  ax = plt.subplot(gs[3, 0]) # row 2
  ax.plot(ts, velocity)
  ax.set_ylabel("velocity [m/s]")

  ax = plt.subplot(gs[4, 0]) # row 4
  ax.plot(ts, evals[:,5])
  ax.set_ylabel("omega [rad/s]")

  ax = plt.subplot(gs[5, 0]) # row 5
  ax.plot(ts, acceleration)
  ax.set_ylabel("acceleration [m/s^2]")

  plt.show()

